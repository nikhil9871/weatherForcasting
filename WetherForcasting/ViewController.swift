//
//  ViewController.swift
//  WetherForcasting
//
//  Created by Nikhil Agarwal on 26/06/17.
//  Copyright © 2017 Nikhil Agarwal. All rights reserved.
//

import UIKit
import GoogleMaps

class ViewController: UIViewController {
    
    //properties
    var currentLocation: CLLocationCoordinate2D?
    let maxZoom: Float = 17
    let locationManager = CLLocationManager()
    var selectedLat = Double()
    var selectedLong = Double()
    var marker = GMSMarker()
    var weatherDetail: WetherModel?
    
    //default location if the location manager can't find the location
    var defaultLocation: CLLocationCoordinate2D = {
        let lat = 28.0
        let long = 77.0
        return CLLocationCoordinate2DMake(lat, long)
    }()
    
    //Selected location when map is dragged
    var selectedLocation: CLLocationCoordinate2D?


    //MARK:- animate to current location else to default location if the current location can not be find
    @IBAction func goToCurrentLocation(_ sender: Any) {
        animateTo(location: currentLocation ?? defaultLocation )
    }
    
    //MARK: On tapping the center locatoin pin
    @IBAction func actionLocationPin(_ sender: Any) {
        guard let selected = self.selectedLocation else {
            return
        }
        
        getForcast(position: selected)
    }

    @IBOutlet var mapView: GMSMapView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mapView.delegate = self
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        locationManager.requestAlwaysAuthorization()
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
    }

    
    func updateSelectedLatAndLong(coordinate:CLLocationCoordinate2D){
        selectedLat = coordinate.latitude
        selectedLong = coordinate.longitude
        self.selectedLocation = CLLocationCoordinate2DMake(selectedLat, selectedLong)
    }
    
    //MARK: Animate to the location passsed
    fileprivate func animateTo(location: CLLocationCoordinate2D) {
        self.mapView.animate(to: GMSCameraPosition(target: location, zoom: Float(maxZoom), bearing: 0, viewingAngle: 0))

    }
    
    
    fileprivate func getForcast(position: CLLocationCoordinate2D) {
        
        guard let url = URL(string: "http://api.openweathermap.org/data/2.5/forecast?lat=\(position.latitude)&lon=\(position.longitude)&appid=82d316e8e5b1aaac532f77fea9e766b4") else {
            return
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = try? JSONSerialization.data(withJSONObject: [:], options: [])
        request.allHTTPHeaderFields = [
            "Content-Type": "application/json"
        ]
        
        let session = URLSession(configuration: .default)
        session.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else { return }
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [String:Any]
                print(json)
                self.weatherDetail = WetherModel(json: json)
                self.goToWeatherDetailScreen()

                
            } catch let error as NSError {
                print(error.description)
            }
            
            print(data)
            
            }.resume()

    }
    
    
    fileprivate func goToWeatherDetailScreen() {
        guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "WeatherViewController") as? WeatherViewController else {
            return
        }
        vc.weatherList = self.weatherDetail?.weatherList
        vc.city = self.weatherDetail?.city
        DispatchQueue.main.async {
            self.navigationController?.pushViewController(vc, animated: true)

        }
    }
    


}

extension ViewController: GMSMapViewDelegate {
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        let newLoc = CLLocation(latitude: position.target.latitude, longitude: position.target.longitude)
        updateSelectedLatAndLong(coordinate: position.target)
        
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        getForcast(position: marker.position)
        return true
    }
    

}


extension ViewController: CLLocationManagerDelegate {
    
    //MARK: LOCATION MANAGER DELEGATE FUNCTIONS
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print(locations[0].coordinate.latitude)
        mapView.isMyLocationEnabled = true
        self.currentLocation = locations[0].coordinate
        animateTo(location: locations[0].coordinate)
        
        locationManager.stopUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        animateTo(location: defaultLocation)


    }
    
    
}
