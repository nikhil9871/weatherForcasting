//
//  WeatherView.swift
//  WetherForcasting
//
//  Created by Nikhil Agarwal on 28/06/17.
//  Copyright © 2017 Nikhil Agarwal. All rights reserved.
//

import UIKit

class WeatherView: UIView {
    var allImagesCache = NSCache<NSString,UIImage>()

    @IBOutlet var date: UILabel!
    @IBOutlet var time: UILabel!
    @IBOutlet var temperature: UILabel!
    @IBOutlet var humidity: UILabel!
    @IBOutlet var pressure: UILabel!
    @IBOutlet var iconWeather: UIImageView!
    
    var weatherDetails: WeatherListItem? {
        didSet {
            if let item = weatherDetails {
                let image = downloadImageAsynchronously(item.weather?.icon ?? "", imageView: iconWeather, placeHolderImage: #imageLiteral(resourceName: "marker"), contentMode: .scaleAspectFill)
                self.iconWeather.image = image
            }
        }
    }
    
    override func awakeFromNib() {
        
    }
    
    
    func downloadImageAsynchronously(_ imageURL:String, imageView:UIImageView, placeHolderImage:UIImage, contentMode:UIViewContentMode) -> UIImage {
        imageView.contentMode = contentMode
        imageView.clipsToBounds = true
        if let cacheImage = self.allImagesCache.object(forKey: "\(imageURL)" as NSString) {//image caching
            return cacheImage
        } else {
            // The image isn't cached, download the img data
            // We should perform this in a background thread
            //        let urlStr : NSString = imageURL.addingPercentEscapes(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))! as NSString
            
            guard let url = NSURL(string: imageURL as String) else { return #imageLiteral(resourceName: "iTunesArtWork")}
            let request: Foundation.URLRequest = Foundation.URLRequest(url: url as URL)
            let mainQueue = OperationQueue.main
            
            NSURLConnection.sendAsynchronousRequest(request, queue: mainQueue, completionHandler: { (response, data, error) -> Void in
                if error == nil {
                    // Convert the downloaded data in to a UIImage object
                    let image = UIImage(data: data!)
                    // Store the image in to our cache
                    if(image != nil) {
                        self.allImagesCache.setObject(image!, forKey: "\(request.url!.absoluteString)" as NSString)
                        // Update the cell
                        DispatchQueue.main.async(execute: {
                            imageView.contentMode = contentMode
                            imageView.image = image
                        })
                    } else {
                        imageView.contentMode = .center
                        imageView.image = placeHolderImage
                    }
                }
                else {
                    print("Error")
                }
            })
        }
        imageView.contentMode = .center
        return placeHolderImage
    }


}
