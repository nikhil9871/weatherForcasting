
//
//  WetherModel.swift
//  WetherForcasting
//
//  Created by Nikhil Agarwal on 28/06/17.
//  Copyright © 2017 Nikhil Agarwal. All rights reserved.
//

import Foundation


class WetherModel: NSObject {
    var city: City?
    var weatherList = [WeatherListItem]()
    
    init(json: [String: Any]) {
        
        if let data = json["city"] as? [String: Any] {
            self.city = City(json: data)
        }
        
        if let list = json["list"] as? [[String: Any]] {
            for item in list {
                let item = WeatherListItem(json: item)
                weatherList.append(item)
            }
        }
    }
}
