//
//  Weather.swift
//  WetherForcasting
//
//  Created by Nikhil Agarwal on 28/06/17.
//  Copyright © 2017 Nikhil Agarwal. All rights reserved.
//

import Foundation
import  UIKit

class  Weather {
    var id: Int?
    var icon: String?

    
    init(json: [String: Any]) {
        
        if let id = json["id"] as? Int {
            self.id = id
        }
        
        if let icon = json["icon"] as? String {
            let iconLink = "https://openweathermap.org/img/w/\(icon).png"
            self.icon = iconLink
        }
     }
}
