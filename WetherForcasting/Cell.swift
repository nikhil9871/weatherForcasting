//
//  Cell.swift
//  WetherForcasting
//
//  Created by Nikhil Agarwal on 29/06/17.
//  Copyright © 2017 Nikhil Agarwal. All rights reserved.
//

import UIKit
import ScalingCarousel

class Cell: ScalingCarouselCell {
    
    //selected temperature type
    var selectedTempType: Temperature? {
        didSet {
            guard let selectedType = selectedTempType else {
                return
            }
            switch selectedType {
            case .Celsius:
                self.temp.text = "Temp: \(celsius ?? 0)"
                break
            case .Fahrenheit:
                self.temp.text = "Temp: \(farhanite ?? 0)"
                break
            case .Kelvin:
                self.temp.text = "Temp: \(kelvin ?? 0)"
                break
                
            default:
                break
            }
        }
    }
    
    
    var allImagesCache = NSCache<NSString,UIImage>()
    
    
    //Temp type
    var celsius: Double?
    var farhanite: Double?
    var kelvin: Double? {
        didSet {
            if let kel = kelvin {
                self.celsius = kToC(K: kel)
                self.farhanite = kToF(K: kel)
            }
        }
    }
    
    
    @IBOutlet var temp: UILabel!
        
    @IBOutlet var humidity: UILabel!
    @IBOutlet var pressure: UILabel!
    @IBOutlet var icon: UIImageView!
    @IBOutlet var date: UILabel!
    
    var weatherDetails: WeatherListItem? {
        didSet {
            if let item = weatherDetails {
                let image = downloadImageAsynchronously(item.weather?.icon ?? "", imageView: icon, placeHolderImage: #imageLiteral(resourceName: "marker"), contentMode: .scaleAspectFill)
                self.icon.image = image
                self.date.text  = unixToDate(unix: item.dt ?? 0)
                self.kelvin = item.main?.temp ?? 0
                self.humidity.text = "Humidity: \(item.main?.humidity ?? 0)"
                self.pressure.text = "Pressure: \(item.main?.pressure ?? 0)"
            }
        }
    }
    
    
    func kToC(K: Double) -> Double {
        let F = (9/5)*(K - 273) + 32
        return F
    }
    
    func kToF(K: Double) -> Double {
        let C = K - 273
        return C
    }
    
    
    override func awakeFromNib() {
        
    }
    
    
    func unixToDate(unix: Double) -> String {
        let date = Date(timeIntervalSince1970: unix)
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT") //Set timezone that you want
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm" //Specify your format that you want
        let strDate = dateFormatter.string(from: date)
        return strDate
        
    }
    
    
    func downloadImageAsynchronously(_ imageURL:String, imageView:UIImageView, placeHolderImage:UIImage, contentMode:UIViewContentMode) -> UIImage {
        imageView.contentMode = contentMode
        imageView.clipsToBounds = true
        if let cacheImage = self.allImagesCache.object(forKey: "\(imageURL)" as NSString) {//image caching
            return cacheImage
        } else {
            // The image isn't cached, download the img data
            // We should perform this in a background thread
            //        let urlStr : NSString = imageURL.addingPercentEscapes(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))! as NSString
            
            guard let url = NSURL(string: imageURL as String) else { return #imageLiteral(resourceName: "iTunesArtWork")}
            let request: Foundation.URLRequest = Foundation.URLRequest(url: url as URL)
            let mainQueue = OperationQueue.main
            
            NSURLConnection.sendAsynchronousRequest(request, queue: mainQueue, completionHandler: { (response, data, error) -> Void in
                if error == nil {
                    // Convert the downloaded data in to a UIImage object
                    let image = UIImage(data: data!)
                    // Store the image in to our cache
                    if(image != nil) {
                        self.allImagesCache.setObject(image!, forKey: "\(request.url!.absoluteString)" as NSString)
                        // Update the cell
                        DispatchQueue.main.async(execute: {
                            imageView.contentMode = contentMode
                            imageView.image = image
                        })
                    } else {
                        imageView.contentMode = .center
                        imageView.image = placeHolderImage
                    }
                }
                else {
                    print("Error")
                }
            })
        }
        imageView.contentMode = .center
        return placeHolderImage
    }
    
}
