//
//  WeatherViewController.swift
//  WetherForcasting
//
//  Created by Nikhil Agarwal on 28/06/17.
//  Copyright © 2017 Nikhil Agarwal. All rights reserved.
//

import UIKit
import ScalingCarousel

enum Temperature {
    case Kelvin, Celsius, Fahrenheit
}


class WeatherViewController: UIViewController {
    
    
    //MARK: Properties
    var weatherList: [WeatherListItem]?
    var city: City?
    var selectedTempType: Temperature = .Kelvin
    var currentTempType: Temperature = .Kelvin
    
    
    //MAKR: IBOutlets
    @IBOutlet var collection: ScalingCarouselView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationItem.title = city?.cityName ?? ""
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Settings", style: .plain, target: self, action:
            #selector(addTapped))
    }
    
    func addTapped() {
        let alertController = UIAlertController(title: "", message: "Format", preferredStyle: .actionSheet)
        let kelvin = UIAlertAction(title: "Kelvin", style: .destructive, handler: {[weak self] (action) -> Void in
            self?.convertTemp()
            self?.selectedTempType = .Kelvin
            self?.collection.reloadData()
        })
        
        let celsius = UIAlertAction(title: "Celsius", style: .destructive, handler: {[weak self] (action) -> Void in
            self?.convertTemp()
            self?.selectedTempType = .Celsius
            self?.collection.reloadData()
        })
        
        let fahrenheit = UIAlertAction(title: "Fahrenheit", style: .destructive, handler: {[weak self] (action) -> Void in
            self?.convertTemp()
            self?.selectedTempType = .Fahrenheit
            self?.collection.reloadData()
        })
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: {[weak self] (action) -> Void in
            self?.dismiss(animated: true, completion: nil)
        })
        
        alertController.addAction(kelvin)
        alertController.addAction(celsius)
        alertController.addAction(fahrenheit)
        present(alertController, animated: true, completion: nil)
    }
    
    func convertTemp() {
        switch currentTempType {
        case .Celsius:
            break
        case .Fahrenheit:
            break
        case .Kelvin:
            break
        }
        
    }
}

extension WeatherViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let items = weatherList else {
            return 0
        }
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
        
        if let scalingCell = cell as? Cell {
            scalingCell.mainView.backgroundColor = .blue
            scalingCell.selectedTempType = self.selectedTempType
            scalingCell.weatherDetails = weatherList![indexPath.row]
        }
        
        return cell
    }
}

extension WeatherViewController: UICollectionViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        collection.didScroll()
    }
}


extension UIView {
    class func fromNib<T : UIView>() -> T {
        return Bundle.main.loadNibNamed(String(describing: T.self), owner: nil, options: nil)![0] as! T
    }
}


