//
//  WeatherMain.swift
//  WetherForcasting
//
//  Created by Nikhil Agarwal on 28/06/17.
//  Copyright © 2017 Nikhil Agarwal. All rights reserved.
//

import Foundation

class WeatherMain {
    var pressure: Double?
    var temp: Double?
    var humidity: Double?
    
    init(json: [String: Any]) {
        
        if let humid = json["humidity"] as? Double {
            self.humidity = humid
        }
        
        if let pressure = json["pressure"] as? Double {
            self.pressure = pressure
        }
        
        if let temperature = json["temp"] as? Double {
            self.temp = temperature
        }
    }
    
}
