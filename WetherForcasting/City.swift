//
//  City.swift
//  WetherForcasting
//
//  Created by Nikhil Agarwal on 28/06/17.
//  Copyright © 2017 Nikhil Agarwal. All rights reserved.
//

import Foundation
import GoogleMaps


class City {
    var cityName: String?
    var coordinates: CLLocationCoordinate2D?
    var country: String?
    
    init(json: [String: Any]) {
        if let name = json["name"] as? String {
            self.cityName = name
        }
        
        if let country = json["country"] as? String {
            self.country = country
        }
        
        if let coordinate = json["coord"] as? [String: Double], let lat = coordinate["lat"] as? Double, let long = coordinate["lon"] as? Double {
            self.coordinates = CLLocationCoordinate2DMake(lat, long)
        }
    }
}
