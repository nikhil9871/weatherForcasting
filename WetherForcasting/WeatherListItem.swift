//
//  WeatherItem.swift
//  WetherForcasting
//
//  Created by Nikhil Agarwal on 28/06/17.
//  Copyright © 2017 Nikhil Agarwal. All rights reserved.
//

import Foundation

class WeatherListItem {
    var dt: Double?
    var weather: Weather?
    var main: WeatherMain?
    
    init(json: [String: Any]) {
        if let date = json["dt"] as? Double {
            self.dt = date
        }
        
        if let weathr = json["main"] as? [String: Any] {
            self.main = WeatherMain(json: weathr)
        }
        
        if let extraWeatehrInfo = json["weather"] as? [[String: Any]] {
            self.weather = Weather(json: extraWeatehrInfo.first ?? [:])
        }
    }
}
